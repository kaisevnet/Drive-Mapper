# Drive-Mapper

## Background
I have several persistant SMB shares that I map on my computers, including Linux computers. When one of my Linux computers loses connection the SMB host, it cripples read/write operations to the home folder, and causes some applications to freeze or crash entirely. This is especially intrusive on laptops which are frequently off-site or off-network

## Purpose
To automatically determine the availability of my SMB servers, and mount or unmount my various network shares accordingly

## Benefits over automount
Absolutely none.

## Usage
Please be aware of any issues listed on the issues page before setting up this script. Non-master branches are to be considered untested

This is a self-installing script runs every minute as a CRON job, and has very little performance impact.

This script must be run as root (for obvious reasons), and so only Root should have write access. Allowing any other users write access to a script that is automatically run as root effectively gives those users full root access to the machine, should they wish to take it

### Configuration
You only need to donload Drive-Mapper.sh. The other files will be generated from this one. Simply run the script with the `setup` flag and it will install the necessary files and walk you through basic configurations

#### Drive-Mapper.sh
This file is automatically copied to `/usr/local/bin` on setup. After running `Drive-Mapper.sh setup` you can delete the original file

#### /etc/drive-mapper/drive-mapper.conf

This file stores unencrypted passwords, and as such is created such that only Root can read it. Please be aware of this when using this script

Any line commented out with a `#` will be ignored, but note that comments work *per line*. Putting a hashtag as anything but the first character will cause the line to be procesed, and it will subsequently fail to read correctly

Every line that is not commented out will be read in the following format (space seperated): `server path username password localdir`

**server**: the IP address or FQDN of the file server (e.g.: for the share `//server/share`, this value should just read `server`)  
**path**: The path of the share on the server including the leading slash (e.g.: for `//server/share`, this value should read `/share`)  
**username**: The username you are using the authenticate  
**password**: The password you are using to authenticate  
**localdir**: The local directory you would like to mount your share to. You cannot enter multiple values (instead, create a duplicate line with with a different localdir set)